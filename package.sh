#!/usr/bin/env bash

function blank() {
    printf "[\e[34mINFO\e[0m] %s\n" $1
}

function separator() {
    blank "------------------------------------------------------------------------"
}

printf "[\e[34mINFO\e[0m] Packaging webapp...\n"
blank

package_type=$1
types=(test test-238 prod)
echo ${types[@]} | grep -wq "${package_type}" || package_type=test

printf "[\e[34mINFO\e[0m] Packaging type is: \e[32m%s\e[0m\n" ${package_type}
blank

printf "[\e[34mINFO\e[0m] Starting...\n"
separator
blank

mvn clean package -f weblogic-pom.xml -Dmaven.test.skip=true -P{package_type}
blank

separator
printf "[\e[34mINFO\e[0m] \e[32mPACKAGING SUCCESS\e[0m\n"
separator
