package org.thraex.pms.question;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author 鬼王
 * @date 2019/09/10 09:26
 */
public class OptionalTest {

    @Test
    public void test1() {
        List<String> list = null;//new ArrayList<>();

        Optional.ofNullable(Arrays.asList('1'))
                .ifPresent(it -> {
                    System.out.println(it);
                    it.stream().forEach(i -> System.out.println(i));
                });
    }

    @Test
    public void test2() {
        Account a = new Account();
        a.setId("Guiwang");

        Predicate<Account> p = s -> s != null;
        if (p.and(s -> s.getId() != null).and(s -> s.getId() == "Guiwang").test(a)) {
            System.out.println(1);
        } else {
            System.out.println(2);
        }
    }

    @Test
    public void test3() {
        List list = new ArrayList();
        Stream.of(Scalar.values()).forEach(it -> list.add(it));
        System.out.println(list);

        Stream.of(Scalar.values())
                .parallel()
                .peek(it -> System.out.println(it));
    }

    @Test
    public void test4() {
        Map<String, Object> con = new HashMap<>();
        con.put("id", 1);
        con.put("name", "鬼王");
        con.put(Scalar.username.name(), "HANZO");

        System.out.println(con.get(Scalar.id.name()));
        System.out.println(con.get(Scalar.name.name()));
        System.out.println(con.get(Scalar.username.name()));

        Stream<Object> stream = Arrays.asList().stream();
        System.out.println(stream);
        System.out.println(stream.toArray());

        long a = 0L;
        System.out.println(a == 0);
        System.out.println(String.valueOf(a).equals("0"));

        System.out.println(Optional.empty());
        //System.out.println(Optional.empty().get());

        System.out.println("gui#1".split("#")[0]);
        System.out.println("gui".split("#")[0]);
    }

    @Test
    public void test5() {
        List<String> list = new ArrayList();
        for (int i = 0; i < 10; i++) {
            list.add(String.valueOf(i));
        }

        Optional.ofNullable(list).ifPresent(it -> {
            List<String> collect = it.parallelStream().map(b -> String.join("#", b, "HANZO")).collect(Collectors.toList());
            System.out.println(collect);
        });

        //Optional<List<Object>> objects = Optional.ofNullable(list).map(it -> Arrays.asList(it, "1", "2"));
        //System.out.println(objects);
    }

    @Test
    public void test6() {
        List<String> collect = Stream.of("Guiwang", "HANZO", "THRAEX")
                .map(it -> String.join("@", it, "xxx.com"))
                .collect(Collectors.toList());
        System.out.println(collect);

        Set<String> collect1 = Stream.of("Guiwang", "HANZO", "THRAEX")
                .map(it -> String.join("@", it, "xxx.com"))
                .collect(Collectors.toSet());
        System.out.println(collect1);

        Map<String, String> collect2 = Stream.of("Guiwang", "HANZO", "THRAEX")
                .map(it -> String.join("@", it, "xxx.com"))
                .collect(Collectors.toMap(k -> k.replace("@xxx.com", ""), v -> v));
        System.out.println(collect2);

        Map<String, String> collect3 = Stream.of("Guiwang", "HANZO", "THRAEX")
                .map(it -> String.join("@", it, "xxx.com"))
                .collect(Collectors.toMap(k -> k.replace("@yyy.com", ""), v -> v, (i, j) -> {
                    System.out.println(i);
                    System.out.println(j);
                    return i + j;
                }));
        System.out.println(collect3);

        Map<Object, Object> collect4 = Stream.of("Guiwang", "HANZO", "THRAEX")
                .map(it -> {
                    //List<String> hanzo = Arrays.asList(it, it.equals("HANZO") ? "" : String.join("@", it, "xxx.com"));
                    String[] hanzo = { it, it.equals("HANZO") ? null : String.join("@", it, "xxx.com") };
                    //System.out.println("total: " + hanzo.toString());
                    return hanzo;
                })
                .collect(Collectors.toMap(it -> {
                    String s = null;
                    //System.out.println("key: " + it);
                    if (it != null) {
                        s = it[0];
                    }

                    System.out.println(s);
                    return s;
                }, it -> {
                    String s = null;
                    //System.out.println("value: " + it);
                    if (it != null) {
                        s = it[1];
                    }

                    System.out.println(s);
                    return s;
                }));
        System.out.println(collect4);
    }

    @Test
    public void test7() {
        String[] s = { "1", "2" };
        this.check(s);
        this.check("Guiwang", "HANZO", "THRAEX");
        String[] z = {};
        this.check(Arrays.asList("a", "b").toArray(z));
    }

    private void check(String... p) {
        System.out.println("Parameters length: " + p.length);
    }

    @Test
    public void test8() {
        try {
            Object a = null;
            String s = Optional.ofNullable(a).map(it -> it.toString()).orElseGet(() -> {
                System.out.println("eles get");
                return "Guiwang";
            });
            System.out.println(s);
            //
            //String s1 = Optional.ofNullable(a).map(it -> it.toString()).orElseThrow(() -> {
            //    System.out.println("else throw");
            //    return new NullPointerException("Object is null....");
            //});
            //System.out.println(s1);

            //Object b = null;
            //System.out.println(Optional.of(b).get());
            //System.out.println(Optional.of(b).map(it -> {
            //    System.out.println(it);
            //    return it.toString();
            //}).orElse("gg"));
            //System.out.println(Optional.ofNullable(b).map(it -> it.toString())
            //        .orElseThrow(() -> new NullPointerException("Object is null.")));

            //String c = "1";
            //System.out.println(Optional.ofNullable(c).orElseThrow(() -> new NullPointerException("String is null.")));
        } catch (Exception e) {
            System.out.println(e.getCause());
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    @Test
    public void test9() {
        Function<String, Boolean> f1 = t -> {
            System.out.println("f1: " + t);

            try {
                if ("gui".equals(t)) {
                    throw new IllegalArgumentException("token error");
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                throw new RuntimeException("null...");
            }


            return false;
        };

        String apply = f1.andThen(t -> {
            System.out.println("f2: " + t);

            return "data";
        }).apply("gui1");
        System.out.println("final: " + apply);

        System.out.println("***************");

        Consumer<String> c1 = p -> {
            System.out.println("c1: " + p);
        };
        c1.andThen(p -> System.out.println("c2: " + p)).andThen(p -> System.out.println("c3: " + p)).accept("Guiwang");
    }

    @Test
    public void test10() {
        Account account = new Account();
        account.setId("1");
        account.setName("HANZO");

        System.out.println("*************************");
        System.out.println("c1: " + account);
        Consumer<Account> consumer = a -> {
            System.out.println("c2: " + a);
            a.setId("2");
            a.setAccount("Guiwang");
        };

        consumer.accept(account);
        System.out.println("c3: " + account);

        System.out.println("*************************");
        System.out.println("f1: " + account);
        Function<Account, Account> function = a -> {
            System.out.println("f2: " + a);
            a.setId("3");
            a.setAccount("THRAEX");
            return a;
        };
        System.out.println("f3: " + function.apply(account));
    }

    @Test
    public void test11() {
        Account a = new Account();
        a.setId("1");
        a.setVersion(1);
        System.out.println("begin: " + a);
        //System.out.println("return:" + this.setInfo(a).get());
        System.out.println(this.setInfo(a).get().equals(a));
        System.out.println(this.setInfo(a).get().equals(this.setInfo(a).get()));
        System.out.println("end..: " + a);

        System.out.println("***********************");
        Account b = new Account();
        b.setId("2");
        b.setVersion(1);
        //System.out.println(this.setInfo(b, "HANZO").get().equals(b));
        //System.out.println(this.setInfo(b, "HANZO").get().equals(this.setInfo(b, "THRAEX").get()));
        Account hanzo = this.setInfo(b, "HANZO").get();
        System.out.println("h: " + hanzo.getName());
        System.out.println(hanzo.equals(b));
        Account thraex = this.setInfo(b, "THRAEX").get();
        System.out.println("ha: " + hanzo.getName());
        System.out.println("t: " + thraex.getName());
        System.out.println(hanzo.equals(thraex));
        System.out.println(b.equals(thraex));
    }

    @Test
    public void test12() {
        String n = "prefix\\\\Guiwang";
        String d = "\\\\";
        String[] split = n.split(d);
        System.out.println(split);
        System.out.println(n.indexOf(d));
        System.out.println(n.lastIndexOf(d));
        System.out.println(n.substring(n.indexOf(d) + d.length()));

        int i = n.lastIndexOf(d);
        System.out.println(i > -1 ? n.substring(i + d.length()) : n);
    }

    public Supplier<Account> setInfo(Account account) {
        return () -> {
            System.out.println("update1...");
            account.setName("鬼王");
            account.setAccount("Guiwang1");
            account.setVersion(2);
            return account;
        };
    }

    public Supplier<Account> setInfo(Account account, String name) {
        return () -> {
            account.setName(name);
            account.setAccount("Guiwang2");
            account.setVersion(2);
            return account;
        };
    }

    @Test
    public void test13() {
        List<String> types = Arrays.asList("application/excel", "application/pdf", "application/rtf");
        String type = "application/pdf";
        //System.out.println(types.contains(type));

        List<String> skips = Arrays.asList("download", "show");
        String path = "/doc/downloadYou/report.do";
        //System.out.println(skips.indexOf(path));

        Boolean aBoolean = skips.parallelStream()
                .map(it -> path.contains(it)).filter(it -> it)
                .findAny()
                .orElse(false);
        //System.out.println(aBoolean);

        boolean b = skips.parallelStream()
                .anyMatch(it -> path.contains(it));
        System.out.println(b);

        System.out.println("***********");
        Predicate<String> hasType = ct -> Stream
                .of("application/excel", "application/pdf", "application/rtf")
                .anyMatch(it -> it.equalsIgnoreCase(ct));
        System.out.println(hasType.test("application/rtf"));
    }

    @Test
    public void test14() {
        //String agent = "Windows 10";
        //String agent = "Linux Desktop";
        String agent = "sdf";

        //Optional<String> any = Stream.of("Windows", "Linux").filter(it -> agent.contains(it)).findAny();
        //System.out.println(any.orElse(null));

        Map<String, String> encodes = new HashMap<String, String>() {
            {
                put("Windows", "GB18030");
                put("Linux", "UTF-8");
                //put("Other", "UTF-8");
            }
        };

        String s = encodes.keySet().parallelStream()
                .filter(it -> agent.contains(it))
                .findAny()
                .map(it -> encodes.get(it)).orElseGet(() -> {
                    System.out.println("set utf-8");
                    return "UTF-8";
                });
        System.out.println(s);
    }

    @Test
    public void test15() {
        byte[] a = null;
        byte[] b = new byte[0];
        System.out.println(a);
        System.out.println(b);
    }

    @Test
    public void test16() {
        int size = 10;
        List<Account> list = new ArrayList<>(size);
        IntStream.range(0, 10).forEach(i ->
                list.add(new Account(String.valueOf(i), "鬼王" + i, "Guiwang" + (i == 2 ? 1 : i))));
        list.stream().forEach(it -> System.out.println(it));

        System.out.println("******************");
        String account = "Guiwang1";
        Account any = list.parallelStream() // account不唯一，则结果不确定
        //Account any = list.stream()
                .filter(it -> it.getAccount().equals(account))
                .peek(it -> System.out.println(it))
                .findAny()
                .orElse(null);
        System.out.println("find: " + any);
    }

    private class Account {

        private String id;

        private String name;

        private String account;

        private int version;

        public Account() { }

        public Account(String id, String name, String account) {
            this.id = id;
            this.name = name;
            this.account = account;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public int getVersion() {
            return version;
        }

        public void setVersion(int version) {
            this.version = version;
        }

        @Override
        public String toString() {
            return "Account{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", account='" + account + '\'' +
                    ", version=" + version +
                    '}';
        }
    }

    private enum Scalar {
        id, name, username, nickname
    }

}
