package org.thraex.pms.question;

import lombok.extern.java.Log;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author 鬼王
 * @Date 2019/08/23 10:32
 */
@Log
public class CheckServlet extends HttpServlet {

    private String p1 = null;
    private String p2 = null;
    private String p3 = null;

    public CheckServlet() {
        System.out.println(1);
        this.p1 = "1";
    }

    @Override
    public void init() throws ServletException {
        log.info(">>>>>>>>>>>>>>>>>> Execute #init()");
        super.init();
        this.p2 = "p2";
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        this.p3 = "p3";
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info(">>>>>>>>>>>>>>>>>> Execute #service(HttpServletRequest req, HttpServletResponse resp)");
        super.service(req, resp);
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        log.info(">>>>>>>>>>>>>>>>>> Execute #service(ServletRequest req, ServletResponse res)");
        super.service(req, res);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info(">>>>>>>>>>>>>>>>>> Execute #doGet(HttpServletRequest req, HttpServletResponse resp)");
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info(">>>>>>>>>>>>>>>>>> Execute #doPost(HttpServletRequest req, HttpServletResponse resp)");

        //resp.setCharacterEncoding("UTF-8");
        //resp.setContentType("text/html");
        resp.setContentType("text/html;charset=UTF-8");

        //PrintWriter writer = resp.getWriter();
        //writer.print("<h1>Hello, Guiwang, 鬼王</h1>");

        ServletOutputStream outputStream = resp.getOutputStream();
        outputStream.print("<h1>Hello, HANZO</h1>");
        //outputStream.print("<h2>鬼王</h2>"); // java.io.CharConversionException: Not an ISO 8859-1 character: 鬼
        outputStream.write("<h2>鬼王</h2>".getBytes());
    }

    @Override
    public void destroy() {
        log.info(">>>>>>>>>>>>>>>>>> Execute #destroy()");
        super.destroy();
    }

}
