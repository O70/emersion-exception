package org.thraex.pms.mock;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 鬼王
 * @date 2019/09/16 08:35
 */
public class IntrospectToken extends HttpServlet {

    private final static String URL = "http://localhost:8081";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json;charset=utf-8");

        System.out.println(URL);

        String token = request.getParameter("token");
        String cid = request.getParameter("client_id");
        String cse = request.getParameter("client_secret");
        System.out.println(token);
        System.out.println(cid);
        System.out.println(cse);

        Map<String, Boolean> result = new HashMap<>(1);
        result.put("active", "Guiwang".equals(token));

        ServletOutputStream os = response.getOutputStream();
        new ObjectMapper().writeValue(os, result);
        os.flush();
        os.close();
    }
}
